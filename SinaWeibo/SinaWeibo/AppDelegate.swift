//
//  AppDelegate.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // 打印用户账户信息
//        printLog(userAccountViewModel.sharedUserAccount.userAccount)
        // 调用网络指示器
        setUpNetwork()
        
        // 注册通知(object - 监听有哪一个对象发出的通知，如果为nil,就是监听所有Name对象发出的通知)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchRootViewController:", name: XGSwiftRootViewControllerNotification, object: nil)
        
        // 设置导航栏和tabbar的外观
        setupNavigationItemAndTabBarItemApperande()
        
        // 设置window的大小
        window = UIWindow(frame:UIScreen.mainScreen().bounds)
        // 指定窗口的背景颜色
        window?.backgroundColor = UIColor.whiteColor()
        // 设置主窗口
//        window?.rootViewController = MainViewController()
//        window?.rootViewController = WelcomeViewController()
//        window?.rootViewController = NewFeatureController()
        window?.rootViewController = defaultRootViewController()
        // 让窗口成为主窗口并显示
        window?.makeKeyAndVisible()
        
//        printLog(isNewVersion())
        
        return true
    }
    
    // MARK: - 移除通知
    deinit {
        // 移除指定的通知,在程序被销毁的时候才会被调用，可以省略
        NSNotificationCenter.defaultCenter().removeObserver(self, name: XGSwiftRootViewControllerNotification, object: nil)
    }
    // 切换控制器通知的监听方法
    func switchRootViewController(notify:NSNotification){
//        printLog(notify)
        
        window?.rootViewController = (notify.object == nil) ? MainViewController() : WelcomeViewController()
    }
    
    // MARK: - 设置启动的默认根控制器
    private func defaultRootViewController() ->UIViewController {
        // 1、判断用户是否登录
        if userAccountViewModel.sharedUserAccount.userLogin {
            // 2、如果登录，判断是否有新的版本
            return isNewVersion() ? NewFeatureController() : WelcomeViewController()
        }
        // 3、如果没有登录，就返回到注册/登录的控制器（MainViewController)
        return MainViewController()
    }
    
    // MARK: - 检查是否有新版本
    private func isNewVersion() ->Bool {
        
        // 1、现在应用程序的版本号
        //①取出沙盒中的版本号
        let bundleVersion = Double(NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String)
//        printLog("当前的版本号 \(bundleVersion)")
        
        // 2、之前保存的应用程序的版本号
        let lastVersionKey = "xg.weibo.version"
        //①去偏好设置里取出之前的版本
        let lastVersion = NSUserDefaults.standardUserDefaults().doubleForKey(lastVersionKey)
//        printLog("之前的版本号 \(lastVersion)")
        
        // 3、保存现在的版本号
        NSUserDefaults.standardUserDefaults().setDouble(bundleVersion!, forKey: lastVersionKey)
        
        // 4、比较两个版本号，返回一个结果
        return bundleVersion > lastVersion
        
    }
    
    ///  设置网络指示器
    private func setUpNetwork(){
        // 设置网络指示器，一旦设置后，在发起网络请求时会在状态栏显示转动的菊花，该指示器只负责AFN的网络请求，其他的一概不负责
        AFNetworkActivityIndicatorManager.sharedManager().enabled = true
        
        // 设置缓存的大小(GET发出的请求时会有缓存的存在（内存缓存4M,磁盘缓存20M）,URLSession只有dataTask会被缓存，downloadTask/uploadTask都不会被缓存
        let cache = NSURLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 20 * 1024 * 1024, diskPath: nil)
        NSURLCache.setSharedURLCache(cache)
        
    }

    // 设置导航栏和tabbar的全局外观
    private func setupNavigationItemAndTabBarItemApperande(){
        // 修改导航栏和tabBar的外观要提前进行设置，一旦设置之后，全局有效
        
        UINavigationBar.appearance().tintColor = UIColor.orangeColor()
        UITabBar.appearance().tintColor = UIColor.orangeColor()
        
    }
    
}

