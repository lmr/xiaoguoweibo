//
//  Status.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/12.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 微博模型
class Status: NSObject {
    /// 微博的创建时间
    var created_at:	String?
    /// 微博的ID
    var id: Int = 0
    /// 微博的信息内容
    var text: String?
    /// 微博的来源
    var source: String? {
        didSet {
            // 一旦给source设置数值之后，立即提取文本链接，并且保存
            // 在此方法中，给本属性设置数值，不会再次调用该方法
            source = source?.href()?.text
        }
    }
    /// 配图URL的字符串数组
    var pic_urls: [[String: String]]?
    /// 用户模型
    var user: User?
    
    /// 如果是原创微博有图，在pic_urls数组中记录
    /// 如果是转发微博有图，在retweet_status.pic_urls数组中记录
    /// 如果转发微博有图，pic_urls数组中就没有图
    /// 被转发的原创微博
    var retweeted_status: Status?
    
    // MARK: - 构造函数
    init(dict: [String: AnyObject]) {
        super.init()
        
        /// 字典转模型
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        /// 1、判断key是否是user
        if key == "user" {
            /// 如果key是user，那么value就是字典
            /// 调用user的构造函数，创建user的对象属性
            user = User(dict: value as! [String: AnyObject])
            /// 此处如果不进行return的话，user的属性会被默认的kvc方法设置成字典
            return
        }
        ///  2、判断key是否是retweeted_status（转发微博)
        if key == "retweeted_status" {
            retweeted_status = Status(dict: value as! [String: AnyObject])
            
            return
        }
        
        
        super.setValue(value, forKey: key)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {    }
    
    override var description: String {
        let keys = ["created_at", "id", "text", "source", "user", "pic_urls", "retweeted_status"]
        
        return dictionaryWithValuesForKeys(keys).description
    }
    
}
