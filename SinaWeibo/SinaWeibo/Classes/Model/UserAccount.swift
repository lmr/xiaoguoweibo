//
//  UserAccount.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/10.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 用户账户信息
class UserAccount: NSObject ,NSCoding{
    // 用于调用access_token，接口获取授权后的access token
    var access_token: String?
    // access_token的生命周期，单位是秒数
    var expires_in:	NSTimeInterval = 0 {
        didSet{
            // 计算过期日期
            expiresDate = NSDate(timeIntervalSinceNow: expires_in)
        }
    }
    // 过期日期
    var expiresDate: NSDate?
     // 当前授权用户的UID。
    var uid: String?
    // 友好显示名称
    var name: String?
    // 用户头像地址（大图），180×180像素
    var avatar_large: String?
    
    init(dict: [String: AnyObject]) {
        super.init()
        
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {  }
    // 对象的描述信息
    override var description: String{
        let keys = ["access_token","expires_in","expiresDate","uid","name","avatar_large"]
        // kvc 模型转字典
        return dictionaryWithValuesForKeys(keys).description
    }
    
    // 保存用户账户信息的路径
     static let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("account.plist")
    
    // 将当前对象归档保存
    func saveUserAccount(){
        // 对象函数中，若要调用静态属性，需用 “类名.属性”
        printLog("保存路径" + UserAccount.path)
        
        // 键值归档
        NSKeyedArchiver.archiveRootObject(self, toFile: UserAccount.path)
    }
    
    // 加载用户账户，返回用户的信息
    class func loadUserAccount() -> UserAccount? {
        // 在解档加载用户账户的时候，需要对token的有效期进行判断
        let account = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? UserAccount
        
        if let date = account?.expiresDate {
            // 对日期进行比较
            if date.compare(NSDate()) == NSComparisonResult.OrderedDescending {
                return account
            }
        }
        return nil
    }
    
    
    // MARK: - NSCoding
    // 归档：将当前对象保存到磁盘之前，转换成二进制数据，跟序列化相似
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeObject(expiresDate, forKey: "expiresDate")
        aCoder.encodeObject(uid, forKey: "uid")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
    }
    
    // 解档：将二进制数据从磁盘加载，转换成自定义对象调用，跟反序列化相似
    required init?(coder aDecoder: NSCoder) {
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        expiresDate = aDecoder.decodeObjectForKey("expiresDate")  as? NSDate
        uid = aDecoder.decodeObjectForKey("uid")  as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large")  as? String
        
    }
    
    
}
