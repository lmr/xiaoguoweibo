//
//  PhotoBrowserController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/29.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import SVProgressHUD
// 可重用cell的标识
private let XGPhotoBrowserCellID = "XGPhotoBrowserCellID"

class PhotoBrowserController: UIViewController {
    
    // MARK: - 控制器的属性
    // 照片的URL数组
    var urls: [NSURL]
    // 用户选择照片的索引
    var selectedIndexPath: NSIndexPath
    // 当前选中的图像索引
    var curretnImageIndex: NSIndexPath {
        return collectionView.indexPathsForVisibleItems().last!
    }
    // 当前选中的图像视图
    var currentImageView: UIImageView {
        
        let cell = collectionView.cellForItemAtIndexPath(curretnImageIndex) as! PhotoBrowserCell
        return cell.imageView
    }
    
    // MARK: - 保存图像
    // 保存当前图像
    @objc private func saveImage() {
        // 1、获取图像
        guard let image = currentImageView.image else {
            SVProgressHUD.showInfoWithStatus("亲，没有图像哦...")
            return
        }
        // 2、保存图像
        UIImageWriteToSavedPhotosAlbum(image, self, "image:didFinishSavingWithError:contextInfo:", nil)
    }
    
    // 判断图片是否保存成功的完成回调方法
    //  - (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
    @objc private func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: AnyObject) {
        let message = (error == nil) ? "恭喜你,保存成功." : "对不起,保存失败!"
        SVProgressHUD.showInfoWithStatus(message)
    }
    
    // MARK: - 构造函数
    ///  构造函数的作用
    ///  1、可以简化外部的调用
    ///  2、可以不适用可选项属性，因此也可以避免后续的解包问题
    init(urls: [NSURL], indexPath: NSIndexPath) {
        self.urls = urls
        selectedIndexPath = indexPath
        
        super.init(nibName: nil, bundle: nil)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        printLog(urls)
        printLog(selectedIndexPath)
    }
    
    ///  视图布局完成，准备开始显示
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // collectionView滚动到用户选择的图片
        collectionView.scrollToItemAtIndexPath(selectedIndexPath, atScrollPosition: .Left, animated: false)
        // 分页控件
        pageControl.currentPage = selectedIndexPath.item
    }
    
    // MARK: - 设置界面
    override func loadView() {
        // 1、创建一个全屏的视图（结构体修改数属性要用var，但对象不需要）
        var rect = UIScreen.mainScreen().bounds
        rect.size.width += 20
        
        view = UIView(frame: rect)
        view.backgroundColor = UIColor.blueColor()
        
        // 2、设置界面的
        setupUI()
    }
    
    // MARK: - 设置界面细节
    private func setupUI() {
        // 1、添加控件
        view.addSubview(collectionView)
        view.addSubview(saveBtn)
        view.addSubview(closeBtn)
        view.addSubview(pageControl)
        
        // 2、设置布局
        collectionView.frame = view.bounds
        
        saveBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        let viewDict = ["saveBtn": saveBtn, "closeBtn": closeBtn]
        // 水平方向的布局
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[closeBtn(==80)]-(>=8)-[saveBtn(==80)]-28-|", options: [], metrics: nil, views: viewDict))
        // 垂直方向的布局
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[closeBtn(35)]-8-|", options: [], metrics: nil, views: viewDict))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[saveBtn(35)]-8-|", options: [], metrics: nil, views: viewDict))
        
        // 分页布局
        view.addConstraint(NSLayoutConstraint(item: pageControl, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: view, attribute: .CenterX, multiplier: 1.0, constant: -10))
        view.addConstraint(NSLayoutConstraint(item: pageControl, attribute: .Bottom, relatedBy: .Equal, toItem: view, attribute: .Bottom, multiplier: 1.0, constant: -8))
        
        // 3、准备控件
        prepareCollectionView()
        preparePageControl()
        
        // 4、监听方法
        closeBtn.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext { [weak self](btn) in
            self?.dismissViewControllerAnimated(true, completion: nil)
        }
        saveBtn.rac_signalForControlEvents(.TouchUpInside).subscribeNext { [weak self] (btn) in
            self?.saveImage()
        }
        pageControl.rac_signalForControlEvents(.ValueChanged).subscribeNext {[weak self] (pageControl) -> Void in
            let indexPath = NSIndexPath(forItem: pageControl.currentPage, inSection: 0)
            // 滚动到指定索引
            self?.collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Left, animated: true)
        }
    
    }
    
    // 准备分页视图
    private func preparePageControl() {
        // 总页数
        pageControl.numberOfPages = urls.count
        // 单页隐藏
        pageControl.hidesForSinglePage = true
        // 分页的颜色
        pageControl.pageIndicatorTintColor = UIColor.greenColor()
        pageControl.currentPageIndicatorTintColor = UIColor.redColor()
    }
  
    /// 准备collectionView
    private func prepareCollectionView() {
        // 1、注册可重用cell 
        collectionView.registerClass(PhotoBrowserCell.self, forCellWithReuseIdentifier: XGPhotoBrowserCellID)
        // 2、数据源
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // 3、设置布局的属性
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.itemSize = view.bounds.size
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        collectionView.pagingEnabled = true
    }
    
    // MARK: - 懒加载控件
    // 集合视图
    private lazy var collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())
    // 保存按钮
    private lazy var saveBtn: UIButton = UIButton(title: "保存", fontSize: 14)
    // 关闭按钮
    private lazy var closeBtn: UIButton = UIButton(title: "关闭", fontSize: 14)
    // 分页控件
    private lazy var pageControl: UIPageControl = UIPageControl()
    /// 照片缩放比例 - Swift 的 extension 中不能包含存储型属性
    private var photoScale: CGFloat = 1

}

// MARK: - collectionView的数据源方法
extension PhotoBrowserController: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        pageControl.currentPage = collectionView.indexPathsForVisibleItems().last?.item ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urls.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(XGPhotoBrowserCellID, forIndexPath: indexPath) as! PhotoBrowserCell
        
//        cell.backgroundColor = UIColor.randomColor()
        cell.url = urls[indexPath.item]
        // 指定cell的缩放代理
        cell.photoDelegate = self
        return cell
    }
}

// MARK: - PhotoBrowserCellDelegate － 照片 Cell 缩放协议
extension PhotoBrowserController: PhotoBrowserCellDelegate {
    
    /// 缩放完成
    func photoBrowserCellEndZoom() {
        
        // 如果缩放比例 < 0.8 dismiss
        if photoScale < 0.8 {
            // 一旦调用了 dismiss 会触发 animator 中的 接触转场动画方法
            // 从当前的动画状态继续完成后续的转场动画
            // 交互式转场动画结束，交给系统的转场
            completeTransition(true)
        } else { // 恢复位置
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.view.transform = CGAffineTransformIdentity
                self.view.alpha = 1.0
                }) { _ in
                    // 动画完成后，显示控件
                    self.hideControllers(false)
            }
        }
    }
    
    /// 缩放中
    ///
    /// - parameter scale: 当前的缩放比例
    func photoBrowserCellDidZooming(scale: CGFloat) {
        print(scale)
        
        // 1. 记录缩放比例
        photoScale = scale
        
        // 2. 显示或者隐藏控件
        hideControllers(scale < 1.0)
        
        // 3. 开始交互转场
        if scale < 1.0 {
            startInteractiveTransition(self)
        } else {
            // 恢复视图的形变参数
            view.transform = CGAffineTransformIdentity
            view.alpha = 1.0
        }
    }
    
    /// 隐藏/显示控件
    private func hideControllers(isHidden: Bool) {
        closeBtn.hidden = isHidden
        saveBtn.hidden = isHidden
        // 分页控件一旦设置了 hidden，单页隐藏无效
        //pageControl.hidden = isHidden
        pageControl.hidden = (urls.count == 1) ? true : isHidden
        
        view.backgroundColor = isHidden ? UIColor.clearColor() : UIColor.blackColor()
        collectionView.backgroundColor = isHidden ? UIColor.clearColor() : UIColor.blackColor()
    }
}

// MARK: - UIViewControllerInteractiveTransitioning - 交互式转场协议
extension PhotoBrowserController: UIViewControllerInteractiveTransitioning {
    
    /// 开始交互式转场
    func startInteractiveTransition(transitionContext: UIViewControllerContextTransitioning) {
        self.view.transform = CGAffineTransformMakeScale(photoScale, photoScale)
        self.view.alpha = photoScale
    }
}

// MARK: - UIViewControllerContextTransitioning - 转场动画上下文协议 - 提供转场动画细节
extension PhotoBrowserController: UIViewControllerContextTransitioning {
    
    /// 完成转场动画
    func completeTransition(didComplete: Bool) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func containerView() -> UIView? { return view.superview }
    
    func isAnimated() -> Bool { return true }
    func isInteractive() -> Bool { return true }
    func transitionWasCancelled() -> Bool { return false }
    func presentationStyle() -> UIModalPresentationStyle { return UIModalPresentationStyle.Custom }
    
    func updateInteractiveTransition(percentComplete: CGFloat) {}
    func finishInteractiveTransition() {}
    func cancelInteractiveTransition() {}
    
    func viewControllerForKey(key: String) -> UIViewController? { return self }
    func viewForKey(key: String) -> UIView? { return view }
    
    func targetTransform() -> CGAffineTransform { return CGAffineTransformIdentity }
    
    func initialFrameForViewController(vc: UIViewController) -> CGRect { return CGRectZero }
    func finalFrameForViewController(vc: UIViewController) -> CGRect { return CGRectZero }
}
