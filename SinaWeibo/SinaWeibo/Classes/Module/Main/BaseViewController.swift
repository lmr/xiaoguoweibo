//
//  BaseViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
/// 功能模块的基类，用来单独处理用户是否登录
class BaseViewController: UITableViewController {
    // 记录用户是否登录
    var userLogin = userAccountViewModel.sharedUserAccount.userLogin
//    var userLogin = false
    // 用户登录视图
    var userView: UserLoginView?
    
    // 如果View不存在，系统会再次调用loadView
    override func loadView() {
        userLogin ? super.loadView() : setupUserLoginView()
    }

    // 设置用户登录出现的视图
    private func setupUserLoginView(){
        userView = UserLoginView()
        // 将根视图替换掉
        view = userView
               
        // 设置导航栏的按钮
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: "userLoginViewWillRegister")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: "userLoginViewWillLogin")
        
        // 设置按钮的监听方法
        userView?.registerBtn.addTarget(self, action: "userLoginViewWillRegister", forControlEvents: UIControlEvents.TouchUpInside)
        userView?.loginBtn.addTarget(self, action: "userLoginViewWillLogin", forControlEvents: UIControlEvents.TouchUpInside)
        
    }
    
    func userLoginViewWillRegister(){
        print("注册")
    }
    
    func userLoginViewWillLogin(){
        let nav = UINavigationController(rootViewController: OauthViewController())
        
        presentViewController(nav, animated: true, completion: nil)
    }
    
}
