//
//  MainViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 添加所有的子控制器，但此处是不会向tabbar中添加按钮的
        // 在 iOS 开发中，懒加载是无处不在的，视图资源只有在需要显示的时候，才会被创建
        addChildViewControllers()
        
//        setupComposeButton()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // 视图将要出现的时候，tabbar中的按钮才会创建
        setupComposeButton()
    }

    // 添加中间发微博的按钮并设置其位置
    private func setupComposeButton(){
        // 1、要先拿到控制器的总个数，用他来计算发微博按钮的位置
        let count = childViewControllers.count
        // 2、计算每个按钮的宽度
        let BtnW = tabBar.bounds.width / CGFloat(count)
        // 3、计算出第一个按钮的位置
        let rect = CGRect(x: 0, y: 0, width: BtnW, height: tabBar.bounds.height)
        //4、设置按钮的位置
        composedBtn.frame = CGRectOffset(rect, 2 * BtnW, 0)

    }
    
    /**
     添加所有的子控制器
     */
    private func addChildViewControllers() {
        // 设置tabbar的渲染颜色
//        tabBar.tintColor = UIColor.orangeColor()
        
        addChildViewController(HomeViewController(),title: "首页",imageName: "tabbar_home")
        addChildViewController(MessageViewController(),title: "消息",imageName: "tabbar_message_center")
        addChildViewController(UIViewController())
        
        addChildViewController(DiscoverViewController(), title: "发现", imageName: "tabbar_discover")
        addChildViewController(ProfileViewController(), title: "我", imageName: "tabbar_profile")
        
    }
    
    /**
     添加单个控制器
     */
    private func addChildViewController(vc: UIViewController, title: String, imageName: String) {
        // 实例化控制器
//        let vc = HomeViewController()
        // 设置标题(这里的title相当于设置了navigationItem和tabbarItem的title)
        vc.title = title
        // 设置图片
        vc.tabBarItem.image = UIImage(named:imageName)
        // 将控制器包装成导航控制器
        let nav = UINavigationController(rootViewController: vc)
        // 添加控制器
        addChildViewController(nav)
    }
    
   
    // MARK: - 用懒加载的方式来创建发微博的按钮
    private lazy var composedBtn:UIButton = {
        //1、创建一个自定义的按钮
        let btn = UIButton()
        //2、设置按钮的图片和背景图片
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: UIControlState.Highlighted)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        //3、将按钮添加到tabbar上
        self.tabBar.addSubview(btn)
        //4、给按钮添加监听方法
        btn.addTarget(self, action:"clickComposedBtn", forControlEvents: UIControlEvents.TouchUpInside)
        
        return btn
    }()
    
    /**
     在iOS中按钮的监听方法，是由运行循环来调用的，因此不能直接使用私有的修饰(private)
     
     但是在swift中，所有的函数如果不使用 private来修饰的话，是全局共享的，但是有的变量是
     不需要外部来调用和修改的，那么用 @objc 关键字能够保证运行循环能够调用，走的 oc 的消
     息机制，调用之前不再判断方法是否存在，一旦和 private 联用后，就能够做到对方法的保护
     */
    @objc private func clickComposedBtn() {
        // 常量又一次设置数据的机会
        let vc: UIViewController
        if userAccountViewModel.sharedUserAccount.userLogin {
            vc = ComposeViewController()
        }else {
            vc = OauthViewController()
        }
        let nav = UINavigationController(rootViewController: vc)
        presentViewController(nav, animated: true, completion: nil)
    }

    
}








