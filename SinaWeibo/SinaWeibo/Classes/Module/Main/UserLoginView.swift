//
//  UserLoginView.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/9.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class UserLoginView: UIView {
    ///  设置用户未登录时的每个界面的信息
    ///
    ///  - parameter imageName: 要显示的图片的名称
    ///  - parameter message:   要现实的消息文字
    func setupUIInfo(imageName:String?, message:String){
        
        msgLabel.text = message;
     // 判断是否需要传递图片，因为第一个界面的图片已经设置过了(如果传递的参数中与图片就说明不是首页)
        if let imgName = imageName {
            iconView.image = UIImage(named: imgName)
            // 需要将第一个界面的图片进行隐藏
            houseView.hidden = true
            // 将遮罩移动到底层，因为这个遮罩是放在iconView的前面的，如果不隐藏，那么其他界面的图片将会被遮罩遮挡一部分
            sendSubviewToBack(coverView)
        }else{
            startAnimation()
        }
    }
    
    // MARK: - 首页的动画效果
    private func startAnimation(){
        let ani = CABasicAnimation(keyPath: "transform.rotation")
        
        ani.toValue = 2 * M_PI
        // 设置动画持续的时间
        ani.duration = 10
        // 设置动画的播放次数
        ani.repeatCount = MAXFLOAT
        // 设置动画执行完毕后，该动画不会被删除
        ani.removedOnCompletion = false
        
        // 将动画添加到iconView中
        iconView.layer.addAnimation(ani, forKey: nil)
    }

    //  MARK: - 界面布局
    // 此方法在使用纯代码开发时会被调用
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }

    // 此方法在使用storyboard开发时会被调用
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    //  设置界面，负责设置每个控件的位置
    private func setupUI(){
     // 1、添加控件
        addSubview(iconView)
        addSubview(coverView)
        addSubview(houseView)
        addSubview(msgLabel)
        addSubview(registerBtn)
        addSubview(loginBtn)
        
        
    // 2. 设置布局，将布局添加到视图上
    // "view1.attr1 = view2.attr2 * multiplier + constant"
    // 默认情况下，使用纯代码开发，是不支持自动布局的，如果要支持自动布局，需要将控件的 translatesAutoresizingMaskIntoConstraints 设置为 false / NO
        // ①图标(第一步：添加整体的约束，第二步：对其Y值进行调整)
        iconView.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX
            , multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -60))
        
        // ②中间的小房子
        houseView.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: houseView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: houseView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -6))
        
        // ③消息文本
        msgLabel.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: msgLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: msgLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 10))
        // 切记：如果要设置一个固定数值，参照的属性，需要设置为 NSLayoutAttribute.NotAnAttribute，参照对象是 nil
        addConstraint(NSLayoutConstraint(item:msgLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 224))
        
        // ④注册按钮
        registerBtn.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: msgLabel, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: msgLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 35))
        
        // ⑤登录按钮
        loginBtn.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: msgLabel, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: msgLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
        addConstraint(NSLayoutConstraint(item: loginBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 35))
        
        //⑥遮罩 - VFL是一种可视化的布局语言
        // H: 水平方向    V: 垂直方向    |: 边界     []: 控件
        // metrics: 极少用   views: [key, VFL 中[] 括起的名称, value: 控件] -> 控件和 VFL 的映射
        coverView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[w]-0-|", options: [], metrics: nil, views: ["w":coverView]))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[w]-(-35)-[btn]", options: [], metrics: nil, views: ["w":coverView,"btn":registerBtn]))
        
        // 设置View的背景颜色（设置背景颜色 用 灰度图 r = g = b，提高程序效率的一个细节，如果能够用颜色表示，就不要用图片）
        backgroundColor = UIColor(white: 237.0 / 255.0, alpha: 1.0)
    }
    
    // MARK: - 用懒加载的方式创建控件
      //  1、图标
    private lazy var iconView: UIImageView = UIImageView(image:UIImage(named:"visitordiscover_feed_image_smallicon" ))
      //  2、中间的小房子
    private lazy var houseView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
      //  3、消息文字
    private lazy var msgLabel: UILabel = {
        // 实例化label
        let label = UILabel()
        
        label.text = "关注一些人，回这里看看有什么惊喜"
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(14)
        label.textColor = UIColor.darkGrayColor()
        label.textAlignment = NSTextAlignment.Center
    
        return label
    }()
        // 4、注册按钮
    lazy var registerBtn: UIButton = {
        let btn = UIButton()
        
        btn.setTitle("注册", forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        
        return btn
    }()
        // 5、登录按钮
    lazy var loginBtn: UIButton = {
        let btn = UIButton()
        
        btn.setTitle("登录", forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        
        return btn
    }()
        // 6、遮罩图片
    private lazy var coverView: UIImageView = UIImageView(image:UIImage(named: "visitordiscover_feed_mask_smallicon"))

    
}
