//
//  WelcomeViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/12.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import SDWebImage

class WelcomeViewController: UIViewController {
    // 头像的底部约束(可选)
    private var iconBottomConstraint:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 设置界面
        setupWelcomeUI()
//        view.backgroundColor = UIColor.blueColor()
        
        // 设置用户的头像
        iconImage.sd_setImageWithURL(userAccountViewModel.sharedUserAccount.avatarURL)
         
    }
    
    // 这个动画要在视图显示完毕后才执行
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // 开始动画
        // ①计算目标的约束值
        let iconH = -(UIScreen.mainScreen().bounds.height + (iconBottomConstraint?.constant)!)
        // ②修改目标的约束值
        iconBottomConstraint?.constant = iconH
        // ③开始动画(带弹性的动画)
        iconLabel.alpha = 0
        UIView.animateWithDuration(1.2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [], animations: { () -> Void in
            // 如果需要就更新布局
            self.view.layoutIfNeeded()
            
            }) { (_) -> Void in
                // 设置头像下面的标签的动画
                UIView.animateWithDuration(0.8, animations: { () -> Void in
                    
                    self.iconLabel.alpha = 1
                    }, completion: { (_) -> Void in
        // 更新根控制器(勿忘：不要直接的在其他地方更新根控制器)
// UIApplication.sharedApplication().keyWindow?.rootViewController = MainViewController()
    // 利用通知来让application来更改更控制器
        NSNotificationCenter.defaultCenter().postNotificationName(XGSwiftRootViewControllerNotification, object: nil)
                        
                })
        }
    }

    // MARK: - 设置界面
    private func setupWelcomeUI() {
        // 1、添加背景图片
        view.addSubview(backImage)
        view.addSubview(iconImage)
        view.addSubview(iconLabel)
        
        // 2、自动布局
        backImage.ff_Fill(view)
        
        let constraints = iconImage.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: view, size: CGSize(width: 90, height: 90), offset: CGPoint(x: 0, y: -200))
        self.iconBottomConstraint = iconImage.ff_Constraint(constraints, attribute: NSLayoutAttribute.Bottom)
        
        iconLabel.ff_AlignVertical(type: ff_AlignType.BottomCenter, referView: iconImage, size: nil, offset: CGPoint(x: 0, y: 16))
        
     }
    
    
    // MARK: - 懒加载控件
    // 1、添加背景图片
    private lazy var backImage:UIImageView = UIImageView(image:UIImage(named: "ad_background"))
    // 2、添加图像
    private lazy var iconImage:UIImageView = {
        let icon = UIImageView(image:UIImage(named: "avatar_default_big"))
        // 设置圆角
        icon.layer.cornerRadius = 45
        icon.layer.masksToBounds = true
        icon.layer.borderWidth = 2
        icon.layer.borderColor = UIColor.greenColor().CGColor
        
        return icon
    }()
    // 3、添加头像下面的标签
    private lazy var  iconLabel = UILabel(title: "欢迎大圣归来", color: UIColor.darkGrayColor(), fontSize: 18)

}
