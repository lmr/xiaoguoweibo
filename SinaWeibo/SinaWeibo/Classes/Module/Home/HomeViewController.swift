//
//  HomeViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewController: BaseViewController {
    
    ///  微博列表数据模型
    private lazy var statusListViewModel = StatusListViewModel()
    ///  Modal动画的提供者
    private lazy var photoBrowserAnimator = PhotoBrowserAnimator()

    override func viewDidLoad() {
        super.viewDidLoad()

        // 用户没有登录的时候
        if !userAccountViewModel.sharedUserAccount.userLogin {
            userView?.setupUIInfo(nil , message: "关注一些人，回这里看看有什么惊喜")
            return
        }
        // 注册通知
        /**
            1、通知的名字
            2、对象，监听发送通知的对象，如果为nil，就监听所有发送该通知的对象
            3、队列，调度block的队列，如果为nil，就在主线程调度block执行
            4、block：接收到通知执行的方法
        */
        NSNotificationCenter.defaultCenter().addObserverForName(XGStatusPictureViewSelectPhotoNotification, object: nil, queue: nil) { [weak self](notification) -> Void in
             
            // 查看通知中的userInfo
            guard let urls = notification.userInfo![XGStatusPictureViewSelectPhotoURLsKey] as? [NSURL] else {
                return
            }
            guard let indexPath = notification.userInfo![XGStatusPictureViewSelectPhotoIndexPahtKey] as? NSIndexPath else {
                return
            }
            // 获取图片
            guard let picView = notification.object as? StatusPictureView else {
                return
            }
            
            // 用modal的方式展现，默认会把上级视图移除
            let vc = PhotoBrowserController(urls: urls, indexPath: indexPath)
            // 1、指定动画的提供者
            vc.transitioningDelegate = self?.photoBrowserAnimator
            // 2、指定modal展现的样式是自定义的
            vc.modalPresentationStyle = UIModalPresentationStyle.Custom
            // 3、计算位置
            let fromRect = picView.screenRect(indexPath)
            let toRect = picView.fullScreenRect(indexPath)
            self?.photoBrowserAnimator.prepareAnimation(picView, fromRect:fromRect, toRect: toRect, url: urls[indexPath.item])
            
            self?.presentViewController(vc, animated: true, completion: nil)
            
        }
        
        prepareTableView()
        
        loadData()
    }
    // 销毁通知
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func prepareTableView() {
        // 注册可重用的cell
        tableView.registerClass(StatusRetweetCell.self, forCellReuseIdentifier: XGStatusRetweetCellID)
        tableView.registerClass(StatusNormalCell.self, forCellReuseIdentifier: XGStatusCellID)
        // tableView的行高
        tableView.estimatedRowHeight = 200
        // 取消tableView的分割线
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        // 准备下拉刷新控件 (控件的高度是60)
        refreshControl = XGRefreshControl()
        refreshControl?.addTarget(self, action: "loadData", forControlEvents: UIControlEvents.ValueChanged)
        
        // 上拉提示控件
        tableView.tableFooterView = pullUpView
        
    }
    
    // 加载数据
    func loadData(){
        // 这个方法只会播放动画，不会刷新数据
        refreshControl?.beginRefreshing()
        
        statusListViewModel.loadStatuses(isPullUpRefresh: pullUpView.isAnimating()).subscribeNext({ (result) -> Void in
            // 此处使用的RAC传递的数值是以NSNumber的形式进行传递的
            let count = (result as! NSNumber).integerValue
            
            self.showPullDownTips(count)
            
            }, error: { (error) -> Void in
                printLog(error)
                
                self.endRefreshData()
                
                SVProgressHUD.showInfoWithStatus("宝贝,您的好像不工作了吧")
            }) {
                
                self.endRefreshData()
                
                // 刷新表格
                self.tableView.reloadData()
        }
    }
    // 下拉刷新条数的提示
    // 切记：NavBar,TabBar,ToolBar 这三个是不能够使用自动布局的
    // 此处千万不要疯狂的进行刷新，否则会出现错误，一旦出现 403 的错误，直接更新下 App ID 就可以了
    private func showPullDownTips(count: Int) {
        
        let title = count == 0 ? "亲,您没有新的微博哦..." : "亲，小二为您刷新到 \(count)条微博"
        let titleHeight: CGFloat = 44
        let rect = CGRect(x: 0, y: -2 * titleHeight, width: UIScreen.mainScreen().bounds.width, height: titleHeight)
        
        pullDownTipLabel.text = title
        pullDownTipLabel.frame = rect
        
        // 让标签以动画的方式出现
        UIView.animateWithDuration(1.2, animations: { () -> Void in
            
            self.pullDownTipLabel.frame = CGRectOffset(rect, 0, 3 * titleHeight)
            }) { (_) -> Void in
                UIView.animateWithDuration(3) { self.pullDownTipLabel.frame = rect }
        }
        
    }
    
    // MARK: - 结束数据的刷新
    private func endRefreshData() {
        // 关闭刷新控件
        self.refreshControl?.endRefreshing()
        // 关闭上拉刷新动画
        self.pullUpView.stopAnimating()
    }
    
    // MARK: - 懒加载上拉刷新控件
    // 下拉提示标签
    private lazy var pullDownTipLabel: UILabel = {
       
        let tipLabel = UILabel(title: nil, color: UIColor.greenColor(), fontSize: 18)
        tipLabel.backgroundColor = UIColor.lightGrayColor()
        tipLabel.textAlignment = NSTextAlignment.Center
        
        self.navigationController?.navigationBar.insertSubview(tipLabel, atIndex: 0)
        
        return tipLabel
    }()
    // 上拉刷新视图
    private lazy var pullUpView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        indicator.color = UIColor.redColor()
        
        return indicator
    }()
}

// MARK：数据源方法，类似于 OC 中的分类，同时将遵守的协议方法分离出来
extension HomeViewController {
    // 表格有多少行
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusListViewModel.statuses.count
    }
    // 每一行要显示的内容
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        ///  ①获取微博的数据
        let viewModel = statusListViewModel.statuses[indexPath.row]
        ///  ②查询可重用的标识符(一定要注册可重用的cell)
        let cell = tableView.dequeueReusableCellWithIdentifier(viewModel.cellID, forIndexPath: indexPath) as! StatusCell
        
        ///  ③设置数据
        cell.statusViewModel = viewModel
        
        ///  ④判断 indexPath 是否是数组中的最后一项，如果是就执行上拉刷新动画
        if (indexPath.row == statusListViewModel.statuses.count - 1) && !pullUpView.isAnimating() {
            printLog("显示上拉刷新。。。")
            pullUpView.startAnimating()
            
            // 开始刷新数据
            loadData()
        }
        
        ///  ⑤设置cell的代理
        cell.cellDelegate = self
        ///  ⑥返回cell
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // 获得模型
        let viewModel = statusListViewModel.statuses[indexPath.row]
        // 判断视图模型的行高是否为0，如果不为0，表示行高已经缓存
        if viewModel.rowHeight > 0 {
            return viewModel.rowHeight
        }
        
        // 获得cell
        let cell = tableView.dequeueReusableCellWithIdentifier(viewModel.cellID) as! StatusCell
        
        // 记录行高
        viewModel.rowHeight = cell.rowHeight(viewModel)
        
        return viewModel.rowHeight
    }
}

extension HomeViewController: StatusCellDelegate {
    func statusCellDidClickURL(url: NSURL) {
        let vc = HomeWebViewController()
        vc.url = url
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

