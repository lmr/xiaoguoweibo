//
//  HomeWebViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/3/5.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class HomeWebViewController: UIViewController {

    var url: NSURL? {
        didSet {
            webView.loadRequest(NSURLRequest(URL: url!))
        }
    }
    
    private lazy var webView = UIWebView()
    
    override func loadView() {
        view = webView
        
        title = "网页"
    }

}