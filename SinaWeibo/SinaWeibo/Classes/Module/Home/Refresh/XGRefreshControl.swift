//
//  XGRefreshControl.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/17.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
 /// 向下拖拽的偏移量，超过-60就自动旋转箭头
private let XGRefreshControlMaxOffset: CGFloat = -60

/// 刷新控件，负责和控制器进行交互
class XGRefreshControl: UIRefreshControl {
    // 停止刷新
    override func endRefreshing() {
        super.endRefreshing()
        
        // 停止动画
        refreshView.stopAnimation()
    }
    
    
    // MARK: - KVO的监听方法
        // 监听对象的key对应的Value的值一旦发生变化，就会调用此方法
    /**
        越向下 Y值越小，当小到一定程度的时候，就会自动进入刷新状态
        越向上，Y值越大，刷新控件始终在视图上
    */
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if frame.origin.y > 0 {
            return
        }
        
        // 判断是否正在刷新
        if refreshing {
            refreshView.loadingAnimation()
            return
        }
        
        if frame.origin.y < XGRefreshControlMaxOffset && !refreshView.rotateFlag {
//            printLog("反过来")
            refreshView.rotateFlag = true
        }else if frame.origin.y >= XGRefreshControlMaxOffset && refreshView.rotateFlag{
//            printLog("转过去")
            refreshView.rotateFlag = false
        }
      
    }
    
    // MARK: - 构造函数
    override init() {
        super.init()
        
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        setupUI()
    }
    
    deinit {
        // 销毁监听
        self.removeObserver(self, forKeyPath: "frame")
    }
    
    private func setupUI() {
        // KVO 监听self.frame
        self.addObserver(self, forKeyPath: "frame", options: [], context: nil)
        
        // 隐藏转轮
        tintColor = UIColor.clearColor()
        
        addSubview(refreshView)
       
        // 自动布局(切记：从XIB中加载的视图会保留XIB中指定的大小
        refreshView.ff_AlignInner(type: ff_AlignType.CenterCenter, referView: self, size: refreshView.bounds.size)
    }
    
    // MARK: - 懒加载控件
    // 调用类函数
    private lazy var refreshView = XGRefreshView.refreshView()
}

/// 刷新控件，单独负责显示内容和动画
class XGRefreshView: UIView {
    // 旋转标记
    private var rotateFlag = false {
        didSet {
            rotatePullDownIconAnimation()
        }
    }
    // 加载图标
    @IBOutlet weak var loadingIcon: UIImageView!
    // 提示视图
    @IBOutlet weak var tipView: UIView!
    // 下拉提示视图
    @IBOutlet weak var pullDownIcon: UIView!
    // 负责从 XIB 加载视图
    class func refreshView() -> XGRefreshView {
        return NSBundle.mainBundle().loadNibNamed("XGRefreshView", owner: nil, options: nil).last! as! XGRefreshView
    }
    // 旋转提示图标动画
    private func rotatePullDownIconAnimation() {
        
        var angle = CGFloat(M_PI)
        angle += rotateFlag ? -0.01 : 0.01
        
        // 在iOS的block动画中，旋转是默认顺时针的，遵循的是就近的原则
        UIView.animateWithDuration(0.5) {
            self.pullDownIcon.transform = CGAffineTransformRotate(self.pullDownIcon.transform, angle)
        }

    }
    // 加载动画
    private func loadingAnimation() {
        // 通过key能够拿到图层上的动画
        let loadingKey = "loadingKey"
        if loadingIcon.layer.animationForKey(loadingKey) != nil {
            return
        }
    
        tipView.hidden = true
        
        let ani = CABasicAnimation(keyPath: "transform.rotation")
        
        ani.toValue = 2 * M_PI
        ani.repeatCount = MAXFLOAT
        ani.duration = 1
        
        loadingIcon.layer.addAnimation(ani, forKey: loadingKey)
    }
    // 停止动画
    private func stopAnimation() {
        tipView.hidden = false
        loadingIcon.layer.removeAllAnimations()
    }
    
    
}