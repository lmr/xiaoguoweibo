//
//  StatusRetweetCell.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/16.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 转发微博的cell
class StatusRetweetCell: StatusCell {
    /// 重写父类的微博数据视图模型（不需要父类的super,只需设置子类的控件内容)
    override var statusViewModel: StatusViewModel? {
        didSet{
            let retweetText = statusViewModel?.retweetText ?? ""
            retweetLabel.attributedText = EmoticonModel.shareViewModel.emoticonText(retweetText, font: retweetLabel.font)
        }
    }
    
    ///  设置UI
    override func setupStatusCellUI() {
        /// 重写父类的方法
        super.setupStatusCellUI()
        /// 添加控件
        contentView.insertSubview(backBtn, belowSubview: pictureView)
        contentView.insertSubview(retweetLabel, aboveSubview: backBtn)
        
        /// 设置布局
        /// ①背景按钮
        backBtn.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabel, size: nil, offset: CGPoint(x: -XGStatusCellMargin, y: XGStatusCellMargin))
        backBtn.ff_AlignVertical(type: ff_AlignType.TopRight, referView: bottomView, size: nil)
        
        /// ②转发文字
        retweetLabel.ff_AlignInner(type: ff_AlignType.TopLeft, referView: backBtn, size: nil, offset: CGPoint(x: XGStatusCellMargin, y: XGStatusCellMargin))
        
        ///  ④配图视图
        let constraint = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: retweetLabel, size: CGSize(width: XGStatusPictureMaxWidth, height: XGStatusPictureMaxWidth), offset: CGPoint(x: 0, y: XGStatusCellMargin))
        /// 记住配图视图的约束
        pictureViewWidthConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Width)
        pictureViewHeightConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Height)
        pictureViewTopConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Top)
        
        /// 设置转发微博的文本代理(一旦设置代理，会调用父类的协议方法)
        retweetLabel.labelDelegate = self
    }
    
    
    // MARK: - 懒加载转发微博的控件
    /// 背景按钮
    private lazy var backBtn: UIButton = {
        let btn = UIButton()
        
        btn.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        
        return btn
    }()
    ///  转发的文字
    private lazy var retweetLabel:FFLabel = FFLabel(title: "", color: UIColor.lightGrayColor(), fontSize: 14, layoutWidth: UIScreen.mainScreen().bounds.width - 2 * XGStatusCellMargin)
   
}
