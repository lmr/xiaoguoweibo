//
//  StatusCellTopView.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import SDWebImage


/// 顶部视图
class StatusCellTopView: UIView {
    
    ///  微博数据视图模型
    var statusViewModel: StatusViewModel? {
        didSet{
            ///  模型数据被设置后，界面的UI会发生变化
            nameLabel.text = statusViewModel?.status.user?.name
            ///  用户头像
            iconView.sd_setImageWithURL(statusViewModel?.userIconURL)
            ///  vip
            vipView.image = statusViewModel?.userVipImage
            ///  会员图片
            memberView.image = statusViewModel?.userMemberImage
           
            // 时间需要不停的计算，每次cell显示时都需要计算
             timeLabel.text = NSDate.sinaDate(statusViewModel?.status.created_at ?? "")?.dateDescription
            // 微博的来源不需要每次都计算
             sourceLabel.text = statusViewModel?.status.source?.href()?.text
        
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // 设置界面
        setupStatusCellTopView()
    }

   required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
    
    // MARK: - 设置顶部的界面
    private func setupStatusCellTopView() {
        
        backgroundColor = UIColor.whiteColor()
        ///  1、添加控件
        addSubview(iconView)
        addSubview(vipView)
        addSubview(nameLabel)
        addSubview(memberView)
        addSubview(timeLabel)
        addSubview(sourceLabel)
        
        
        ///  2、设置布局
        let offset = CGPoint(x: XGStatusCellMargin, y: 0)
        iconView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: self, size: CGSize(width: XGStatusIconWH, height: XGStatusIconWH), offset: CGPoint(x: XGStatusCellMargin, y: XGStatusCellMargin))
        
        nameLabel.ff_AlignHorizontal(type: ff_AlignType.TopRight, referView: iconView, size: nil, offset: offset)
        
        memberView.ff_AlignHorizontal(type: ff_AlignType.TopRight, referView: nameLabel, size: nil, offset: offset)
        
        timeLabel.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: iconView, size: nil, offset: offset)
        
        sourceLabel.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: timeLabel, size: nil, offset: offset)
        
        vipView.ff_AlignInner(type: ff_AlignType.BottomRight, referView: iconView, size: nil, offset: CGPoint(x: 8, y: 8))
    }
    
    // MARK: - 懒加载控件
    private lazy var iconView: UIImageView = UIImageView(image: UIImage(named: "avatar_default_big"))
    private lazy var nameLabel = UILabel(title:"姓名", color: UIColor.darkGrayColor(), fontSize: 14)
    private lazy var memberView: UIImageView = UIImageView(image: UIImage(named: "common_icon_membership_level1"))
    private lazy var timeLabel = UILabel(title: "刚刚", color: UIColor.orangeColor(), fontSize: 10)
    private lazy var sourceLabel = UILabel(title: "来自 新浪微博", color: UIColor.darkGrayColor(), fontSize: 10)
    private lazy var vipView: UIImageView = UIImageView(image: UIImage(named: "avatar_grassroot"))
}
