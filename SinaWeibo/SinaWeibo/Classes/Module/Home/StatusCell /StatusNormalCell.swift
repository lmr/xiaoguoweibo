//
//  StatusNormalCell.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/17.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 原创微博的cell
class StatusNormalCell: StatusCell {

    override func setupStatusCellUI() {
        super.setupStatusCellUI()
     
        ///  ④配图视图
        let constraint = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabel, size: CGSize(width: XGStatusPictureMaxWidth, height: XGStatusPictureMaxWidth), offset: CGPoint(x: 0, y: XGStatusCellMargin))
        /// 记住配图视图的约束
        pictureViewWidthConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Width)
        pictureViewHeightConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Height)
        pictureViewTopConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Top)

    }

}
