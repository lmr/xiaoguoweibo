//
//  StatusCellBottomView.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class StatusCellBottomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        ///  设置界面
        setupStatusCellBottomView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    ///  设置cell底部的界面
    private func setupStatusCellBottomView() {
        // 设置背景颜色
//        backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        backgroundColor = UIColor.yellowColor()
        
        // 添加控件 
        addSubview(retweetBtn)
        addSubview(commentBtn)
        addSubview(likeBtn)
        
        // 水平平铺三个按钮
        ff_HorizontalTile([retweetBtn, commentBtn, likeBtn], insets: UIEdgeInsetsZero)
        
    }
    
    // MARK: - 懒加载控件
    private lazy var retweetBtn: UIButton = UIButton(title: "  转发", imageName: "timeline_icon_retweet", color: UIColor.darkGrayColor(), fontSize: 14)
    private lazy var commentBtn: UIButton = UIButton(title: "  评论", imageName: "timeline_icon_comment", color: UIColor.darkGrayColor(), fontSize: 14)
    private lazy var likeBtn: UIButton = UIButton(title: "  赞", imageName: "timeline_icon_unlike", color: UIColor.darkGrayColor(), fontSize: 14)
}
