//
//  StatusCell.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 每个控件之间的间距
let XGStatusCellMargin: CGFloat = 12
/// 头像的大小
let XGStatusIconWH: CGFloat = 35
/// 默认的图片大小
let XGStatusPictureItemWH: CGFloat = 90
/// 默认的图片间距
let XGStatusPictureItemMargin: CGFloat = 5
/// 每行的最大图片数量
let XGStatusPictureMaxCount: CGFloat = 3
/// 配图视图的最大尺寸
let XGStatusPictureMaxWidth = XGStatusPictureMaxCount * XGStatusPictureItemWH + (XGStatusPictureMaxCount - 1) * XGStatusPictureItemMargin

protocol StatusCellDelegate: NSObjectProtocol {
    func statusCellDidClickURL(url: NSURL)
}

/// 微博的cell
class StatusCell: UITableViewCell {
    
    weak var cellDelegate: StatusCellDelegate?
    
    ///  微博数据视图模型
    var statusViewModel: StatusViewModel? {
        didSet{
            ///  模型数据被设置后，界面的UI会发生变化
            topView.statusViewModel = statusViewModel
            /// 微博正文
            let statusText = statusViewModel?.status.text ?? ""
            contentLabel.attributedText = EmoticonModel.shareViewModel.emoticonText(statusText, font: contentLabel.font)
            /// 设置配图视图
            pictureView.statusViewModel = statusViewModel
            
            // 计算配图视图的大小
            pictureViewWidthConstraint?.constant = pictureView.bounds.width
            pictureViewHeightConstraint?.constant = pictureView.bounds.height
            // 根据是否有配图来设置顶部的约束
            pictureViewTopConstraint?.constant = statusViewModel?.thumbnailURLs?.count == 0 ? 0 : XGStatusCellMargin
        }
    }
    
    // 宽度约束
    var pictureViewWidthConstraint: NSLayoutConstraint?
    // 高度约束
    var pictureViewHeightConstraint: NSLayoutConstraint?
    // 配图视图的顶部约束
    var pictureViewTopConstraint: NSLayoutConstraint?
    
    // 计算指定视图模型对应的行高
    func rowHeight(viewModel: StatusViewModel) -> CGFloat {
        // 设置视图模型
        statusViewModel = viewModel
        // 更新约束
        layoutIfNeeded()
        // 返回底部视图的最大高度
        return CGRectGetMaxY(bottomView.frame)
    }
    
    // MARK: - 搭建界面
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // 设置界面
        setupStatusCellUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 设置界面
     func setupStatusCellUI() {
        // 表格的cell一定要指定背景颜色，同时不要指定clearColor,也不要使用透明度
        backgroundColor = UIColor.whiteColor()
        
        // 0、添加顶部分隔条
        let topSepView = UIView()
        topSepView.backgroundColor = UIColor.greenColor()
        
        // 1、添加控件
        contentView.addSubview(topSepView)
        contentView.addSubview(topView)
        contentView.addSubview(contentLabel)
        contentView.addSubview(pictureView)
        contentView.addSubview(bottomView)
        
        // 2、设置布局(切记：cell的默认宽度是320，高度是44，所以宽度不能直接使用cell的宽度)
        let screenWidth = UIScreen.mainScreen().bounds.width
         ///  ①顶部分割线
        topSepView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: contentView, size: CGSize(width: screenWidth, height: 5))
        
         ///  ②顶部视图
        topView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: topSepView, size: CGSize(width: screenWidth, height: XGStatusIconWH + XGStatusCellMargin))
        
         ///  ③文本标签
        contentLabel.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: topView, size: nil, offset: CGPoint(x: XGStatusCellMargin, y: XGStatusCellMargin))
        
//         ///  ④配图视图
//        let constraint = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabel, size: CGSize(width: XGStatusPictureMaxWidth, height: XGStatusPictureMaxWidth), offset: CGPoint(x: 0, y: XGStatusCellMargin))
//         /// 记住配图视图的约束
//        pictureViewWidthConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Width)
//        pictureViewHeightConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Height)
//        pictureViewTopConstraint = pictureView.ff_Constraint(constraint, attribute: NSLayoutAttribute.Top)
        
         ///  ⑤底部视图
        bottomView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: pictureView, size: CGSize(width: screenWidth, height: 44), offset: CGPoint(x: -XGStatusCellMargin, y: XGStatusCellMargin))
        
        // 指定底部视图相对于底边的约束
//        bottomView.ff_AlignInner(type: ff_AlignType.BottomRight, referView: contentView, size: nil)
        
        // 指定label的代理
        contentLabel.labelDelegate = self
    }

    // MARK: - 懒加载cell中的控件
    /// 1、顶部显示的内容
    private lazy var topView: StatusCellTopView = StatusCellTopView()
    /// 2、文本标签
    lazy var contentLabel:FFLabel = FFLabel(title:nil, color: UIColor.darkGrayColor(), fontSize: 15, layoutWidth: UIScreen.mainScreen().bounds.size.width - 2 * XGStatusCellMargin)
    /// 3、配图视图
    lazy var pictureView: StatusPictureView = StatusPictureView()
    /// 4、底部显示的内容
    lazy var bottomView: StatusCellBottomView = StatusCellBottomView()

}

// MARK: - FFLabelDelegate
extension StatusCell: FFLabelDelegate {
    
    func labelDidSelectedLinkText(label: FFLabel, text: String) {
        // 1、判断是否是以 http://开头
        if !text.hasPrefix("http://") {
            return
        }
        
        // 2、根据判断中的text来生成一个URL 
        guard let url = NSURL(string: text) else {
            return
        }
        
        // 3、通过代理通知控制器
        cellDelegate?.statusCellDidClickURL(url)
    }
    
    
}