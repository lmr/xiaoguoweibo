//
//  ProfileViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        userView?.setupUIInfo("visitordiscover_image_profile" , message: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
//        view.backgroundColor = UIColor.purpleColor()
    }

}
