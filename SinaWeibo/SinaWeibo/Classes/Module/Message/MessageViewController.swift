//
//  MessageViewController.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/8.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        userView?.setupUIInfo("visitordiscover_image_message", message: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
        
//       view.backgroundColor = UIColor.greenColor()
    }

}
