//
//  userAccountViewModel.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/10.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit
import ReactiveCocoa

/// 用户账户的视图模型
class userAccountViewModel: NSObject {

    // 单例
    static let sharedUserAccount = userAccountViewModel()
    
    override init() {
        userAccount = UserAccount.loadUserAccount()
    }
    
    // 用户账户
    var userAccount: UserAccount?
    
    // accessToken 
    var accessToken: String? {
        
        return userAccount?.access_token
    }
    // 用户登录标记
    var userLogin: Bool {
        return accessToken != nil
    }
    // 用户头像
    var avatarURL:NSURL?{
        return NSURL(string: userAccount?.avatar_large ?? "") 
    }
    
    // MARK: - 加载网络数据
    func loadUserAccount(code: String) -> RACSignal {
        
        return RACSignal.createSignal({ (subscriber) -> RACDisposable! in
            NetworkTools.sharedTools.loadAccessToken(code).doNext({ (result) -> Void in
//                printLog(result)
                // 创建用户账户模型
                let account = UserAccount(dict: result as! [String : AnyObject])
                printLog(account)
                // 设置当前账户的属性
                self.userAccount = account
                
                NetworkTools.sharedTools.loadUserInfo(account.uid!).subscribeNext({ (result) -> Void in
                    
                    let dict = result as! [String: AnyObject]
                    // 设置账号的属性
                    account.name = dict["name"] as? String
                    account.avatar_large = dict["avatar_large"] as? String
                    
                    printLog(account)
                    // 保存账号
                    account.saveUserAccount()
                    
                    // 通知订阅者网络数据加载完成
                    subscriber.sendCompleted()
                    
                    }, error: { (error) -> Void in
                        subscriber.sendError(error)
                    })
            
          }).subscribeError({ (error) -> Void in
                subscriber.sendError(error)
        })
            return nil
        })
        
        
    }

    
    
}
