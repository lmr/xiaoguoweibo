//
//  StatusViewModel.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 原创微博cell的可重用标识符
let XGStatusCellID = "XGStatusCellID"
/// 转发微博的可重用cell
let  XGStatusRetweetCellID = "XGStatusRetweetCellID"

/// 微博的视图模型，为界面提供显示时使用
class StatusViewModel: NSObject {
    ///  微博对象
    var status: Status
    ///  当前模型对应的行高
    var rowHeight: CGFloat = 0
    /// 返回当前视图模型对应的可重用标识符
    var cellID: String {
        return status.retweeted_status != nil ? XGStatusRetweetCellID : XGStatusCellID
    }
    
    ///  被转发的原创微博的文字格式：@作者：原文
    var retweetText: String? {
        let username = status.retweeted_status?.user?.name ?? ""
        let text = status.retweeted_status?.text ?? ""
        
        return "@\(username):\(text)"
    }
    
    /// 用户头像的URL
    var userIconURL: NSURL? {
        return NSURL(string: status.user?.profile_image_url ?? "")
    }
    /// 认证类型 -1：没有认证，0，认证用户，2,3,5: 企业认证，220: 达人
    var userVipImage: UIImage? {
        switch (status.user?.verified ?? -1) {
        case 0: return UIImage(named: "avatar_vip")
        case 2, 3, 5: return UIImage(named: "avatar_enterprise_vip")
        case 220: return UIImage(named: "avatar_grassroot")
        default: return nil
        }
        
    }
    /// 会员等级 1-6
    var userMemberImage: UIImage? {
        if status.user?.mbrank > 0 && status.user?.mbrank < 7 {
            return UIImage(named: "common_icon_membership_level\(status.user!.mbrank)")
        }
        return nil
    }
    /// 如果是原创微博有图，在pic_urls数组中记录
    /// 如果是转发微博有图，在retweet_status.pic_urls数组中记录
    /// 如果转发微博有图，pic_urls数组中就没有图
    ///  配图缩略图的URL数组
    var thumbnailURLs: [NSURL]?
    ///  中等尺寸的图像的URL数组(用户不一定会查看所有的图片)
    var bmiddleURLs: [NSURL]? {
        // 1、判断thumbnailURLs是否为空
    guard let urls = thumbnailURLs else {
        return nil
    }
        // 2、顺序替换每一个url字符串中的字符
        var array = [NSURL]()
        for url in urls {
            let urlString = url.absoluteString.stringByReplacingOccurrencesOfString("/thumbnail/", withString: "/bmiddle/")
            
            array.append(NSURL(string: urlString)!)
        }
        return array
}
    
    // MARK: - 构造函数
    init(status: Status) {
        self.status = status
        ///  如果是转发微博，就去retweet_status 的pic_urls,否则直接取pic_urls
//        if let urls = (status.retweeted_status?.pic_urls != nil) ? status.retweeted_status?.pic_urls :status.pic_urls {
        if let urls = status.retweeted_status?.pic_urls ?? status.pic_urls {
        
            thumbnailURLs = [NSURL]()
            for dict in urls {
                thumbnailURLs?.append(NSURL(string: dict["thumbnail_pic"]!)!)
            }
        }
        
//        ///  给缩略图数组设置数值
//        /// 判断是否有图像
//        if status.pic_urls != nil {
//            ///  实例化缩略图的数组
//            thumbnailURLs = [NSURL]()
//            ///  遍历数组，插入URL
//            for dict in status.pic_urls! {
//                thumbnailURLs?.append(NSURL(string: dict["thumbnail_pic"]!)!)
//            }
//            
//        }
//        ///  被转发的原创微博中有图，status.pic_urls 中一定没有图
//        if status.retweeted_status?.pic_urls != nil {
//            ///  遍历数组，插入url
//            for dict in status.retweeted_status!.pic_urls! {
//                thumbnailURLs?.append(NSURL(string: dict["thumbnail_pic"]!)!)
//            }
//        }
        
        super.init()
    }
    
    override var description: String {
        return status.description + "缩略图URL数组 \(thumbnailURLs)"
    }
    
}
