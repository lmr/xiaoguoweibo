//
//  UILabel+Extension.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/13.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

extension UILabel {
    ///  便利构造函数
    ///  - parameter title:       文字的内容
    ///  - parameter color:       字体颜色
    ///  - parameter fontSize:    字体大小
    ///  - parameter layoutWidth: 文本的布局宽度
    ///  - returns: 返回一个UILabel
    convenience init(title: String?, color: UIColor, fontSize: CGFloat, layoutWidth: CGFloat = 0){
        // 实例化当前对象
        self.init()
        
        // 设置对象的属性
        text = title
        textColor = color
        font = UIFont.systemFontOfSize(fontSize)
        
        if layoutWidth > 0 {
            preferredMaxLayoutWidth = layoutWidth
            numberOfLines = 0
        }
        sizeToFit()
    }
    
}