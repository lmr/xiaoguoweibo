//
//  UIBarButton+Extension.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/19.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    ///  便利构造函数
    ///
    ///  - parameter imageName:  imageName
    ///  - parameter target:     target
    ///  - parameter actionName: actionName
    ///
    ///  - returns: UIBarButton
     convenience init(imageName: String, target: AnyObject?, actionName: String?) {
        
        let btn = UIButton(imageName: imageName)
        // 添加监听方法
        if target != nil && actionName != nil {
            btn.addTarget(target, action: Selector(actionName!), forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        self.init(customView: btn)
    }
    
    
}
