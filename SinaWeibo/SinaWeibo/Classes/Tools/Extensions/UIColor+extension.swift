//
//  UIColor+extension.swift
//  SinaWeibo
//
//  Created by 小果 on 16/2/29.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

extension UIColor {
    ///  创建随机颜色
    ///
    ///  - returns: 返回一个随机的颜色
    class func randomColor() -> UIColor {
        let r = CGFloat(random() % 256) / 255
        let g = CGFloat(random() % 256) / 255
        let b = CGFloat(random() % 256) / 255
        
      return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
}
