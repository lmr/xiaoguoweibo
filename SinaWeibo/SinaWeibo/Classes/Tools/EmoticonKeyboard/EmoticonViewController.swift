//
//  EmoticonViewController.swift
//  表情键盘
//
//  Created by 小果 on 16/2/23.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

// 可重用的标识符
private let EmoticonCellID = "EmoticonCellID"

// 表情键盘的控制器 —— 系统键盘的默认高度是216
class EmoticonViewController: UIViewController {
    
    ///  选中表情的闭包回调
    var selectEmoticonCallBack: (emoticon: Emoticon)->()
    
    init(selectEmoticon: (emoticon: Emoticon)-> ()) {

        selectEmoticonCallBack = selectEmoticon
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 监听方法
    @objc private func clickItem(item: UIBarButtonItem) {
        
        let indexPath = NSIndexPath(forRow: 0, inSection: item.tag)
        
        // 让collection滚动到相应的位置
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Left, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    view.backgroundColor = UIColor.redColor()
    
        // 加载表情数据
//        viewModel.loadPakages()
        setupUI()
        
    }
    // MARK: - 设置界面
    private func setupUI() {
        // 1、添加控件
        view.addSubview(collectionView)
        view.addSubview(toolBar)
        
        // 2、自动布局
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        let ViewDict = ["tb": toolBar, "cv": collectionView]
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[cv]-0-|", options: [], metrics: nil, views: ViewDict))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[tb]-0-|", options: [], metrics: nil, views: ViewDict))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[cv]-[tb(44)]-0-|", options: [], metrics: nil, views: ViewDict))
        
        // 3、准备控件
        prepareToolBar()
        prepareCollectionView()
    }
    
    // MARK: - 准备工具栏
    private func prepareToolBar() {
        // 按钮默认的颜色是系统的蓝色，需要修改
        toolBar.tintColor = UIColor.darkGrayColor()
        
        var items = [UIBarButtonItem]()
        
        var index = 0
        for p in viewModel.pakages {
            items.append(UIBarButtonItem(title: p.group_name_cn, style: .Plain, target: self, action: "clickItem:"))
            items.last?.tag = index++
            
            items.append(UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil))
        }
        // 删除最后的一个弹簧
        items.removeLast()
        
        toolBar.items = items
        
    }
    
    // MARK: - 准备表情界面
    private func prepareCollectionView() {
        
        collectionView.backgroundColor = UIColor.whiteColor()
        // 1、注册cell
        collectionView.registerClass(EmoticonCell.self, forCellWithReuseIdentifier: EmoticonCellID)
        
        // 2、指定数据源和代理
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    // MARK: - 懒加载控件
    // 工具栏
    private lazy var toolBar = UIToolbar()
    // collectionView
    private lazy var collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: EmoticonViewLayout())
    // 表情包的视图模型
    private lazy var viewModel = EmoticonModel.shareViewModel
    
}

// MARK: - 表情键盘的布局
private class EmoticonViewLayout: UICollectionViewFlowLayout {
    
    // 准备布局，在第一次使用的时候会被调用(collectonView的大小已经确定，即已经完成了自动布局)
    // 准备布局的方法，会在数据源(cell 的个数）前被调用，可以再次准备layout的相关属性
    private override func prepareLayout() {
        // 计算每一个表情的宽度
        let w = collectionView!.bounds.size.width / 7
        // 每一行的间距（如果写0.5的话，在iPhone5上显示就会出现异常,每页只显示两行，以为浮点数的计算为产生偏差)
        let margin = (collectionView!.bounds.height - 3 * w) * 0.499
        itemSize = CGSize(width: w, height: w)
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        
        sectionInset = UIEdgeInsets(top: margin, left: 0, bottom: margin, right: 0)
        // 设置滚动的方向
        scrollDirection = UICollectionViewScrollDirection.Horizontal
        // 是否分页
        collectionView?.pagingEnabled = true
        // 隐藏水平滚动条
        collectionView?.showsHorizontalScrollIndicator = false
    }
    
}

// MARK: - 数据源方法
extension EmoticonViewController: UICollectionViewDataSource,UICollectionViewDelegate {
    
    // 组的数量
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return viewModel.pakages.count
    }
    // 每一个 section 对应的表情包中包含的表情数量
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.pakages[section].emoticons.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(EmoticonCellID, forIndexPath: indexPath) as! EmoticonCell
//        cell.backgroundColor = (indexPath.item % 2 == 0) ? UIColor.redColor() : UIColor.greenColor()
        
        // 设置表情包索引
        cell.emoticon = viewModel.emoticon(indexPath)
        return cell
    }
    
    // 选中cell的代理方法
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // 执行闭包回调
        selectEmoticonCallBack(emoticon: viewModel.emoticon(indexPath))
        
        // 添加最近表情符号
        viewModel.favourite(indexPath)
        
        // 刷新collectionView的第0组(用这种方法会影响用户的体验，会出现闪动的现象，且表情的位置会发生变化)
//        collectionView.reloadSections(NSIndexSet(index: 0))
    }
}

// MARK: - 表情cell
private class EmoticonCell: UICollectionViewCell {
    
    // 表情包对应的索引
    var emoticon: Emoticon? {
        didSet {
            // 设置模型（清空可以解决重用的问题）
            // 1、图片(如果没有会清空图片)
            emoticonBtn.setImage(UIImage(contentsOfFile: emoticon!.imagePath), forState: UIControlState.Normal)
        
            // 2、emoji(如果没有会清空文字)
            emoticonBtn.setTitle(emoticon!.emoji, forState: .Normal)
            
            // 3、添加删除按钮
            if emoticon!.isRemove {
                emoticonBtn.setImage(UIImage(named: "compose_emotion_delete"), forState: .Normal)
                emoticonBtn.setImage(UIImage(named: "compose_emotion_delete_highlighted"), forState: .Highlighted)
            }
        
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(emoticonBtn)
        emoticonBtn.backgroundColor = UIColor.whiteColor()
        // CGRectInset返回相同中心点的矩形（切记：此处一定要使用bounds）
        emoticonBtn.frame = CGRectInset(bounds, 4, 4)
        // 设置字体
        emoticonBtn.titleLabel?.font = UIFont.systemFontOfSize(32)
        // 禁用按钮
        emoticonBtn.userInteractionEnabled = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 懒加载控件
    private lazy var emoticonBtn: UIButton = UIButton()
}



