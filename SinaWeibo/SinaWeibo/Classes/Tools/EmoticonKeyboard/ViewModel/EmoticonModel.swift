//
//  EmoticonModel.swift
//  表情键盘
//
//  Created by 小果 on 16/2/24.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/* 表情的视图模型(用来加载表情数据),从Emoticon.bundle中读取emoticons.plist,遍历packes数组，创建EmoticonPakage数组，EmoticonPakage的明细内容，从id对应的目录加载info.plist完成字典转模型
    在swift中一个对象可以不继承NSObject，如果继承NSObject可以用kVC的方式给属性赋值，（切记：如果是模型对象
    最好还是继承NSObject
    如果对象没有属性，或者不依赖KVC,可以建立一个没有父类的对象，同时这也说明该对象的重量级比较轻，内存消耗比较小
*/
class EmoticonModel {

    /// 单例
    static let shareViewModel = EmoticonModel()
    /// 构造函数
    private init() {
        ///  加载表情包
        loadPakages()
    }
    ///  表情包的数组
    lazy var pakages = [EmoticonPakage]()
    
    /// 根据给定的字符串，生成带表情符号的属性字符串
    func emoticonText(str: String, font: UIFont) -> NSAttributedString {
        let pattern = "\\[.*?\\]"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        
        // matchesInString 查找 pattern 所有的匹配项
        let results = regex.matchesInString(str, options: [], range: NSRange(location: 0, length: str.characters.count))
        
        // 获得数组数量
        var count = results.count
        
        // 准备属性字符串
        let strM = NSMutableAttributedString(string: str)
        
        // 根据数量`倒序`遍历数组内容
        while count > 0 {
            let range = results[--count].rangeAtIndex(0)
            
            // 根据 range 获取到对应的 chs 字符串
            let chs = (str as NSString).substringWithRange(range)
            
            // 根据 chs 获得对应的 emoticon 对象
            if let emoticon = emoticon(chs) {
                
                let imageText = EmoticonAttachment.emoticonAttributeText(emoticon, font: font)
                
                // 替换 strM 中对应的属性文本
                strM.replaceCharactersInRange(range, withAttributedString: imageText)
            }
        }
        
        return strM
    }
    
    /// 根据字符串查找对应的表情符号
    private func emoticon(str: String) -> Emoticon? {
        
        var emoticon: Emoticon?
        
        for p in pakages {
            
            // 从 p.emoticons 数组中`过滤`出指定字符串的表情
            //            emoticon = p.emoticons.filter({ (em) -> Bool in
            //                return em.chs == str
            //            }).last
            
            emoticon = p.emoticons.filter() { $0.chs == str }.last
            
            // 如果找到 emoticon 直接退出
            if emoticon != nil {
                break
            }
        }
        
        return emoticon
    }

    
    ///  添加最近的表情
    func favourite(indexPath: NSIndexPath) {
        // 如果是第0个分组，就不参与排序
        if indexPath.section == 0 {
            return
        }
        
        // 1、获取表情符号
        let emot = emoticon(indexPath)
        // 对表情的使用次数进行列加
        emot.times++
        // 2、将表情符号添加到“最近”的那一组的首位
        // 判断是否已经存在表情
        if !pakages[0].emoticons.contains(emot) {
            
            pakages[0].emoticons.insert(emot, atIndex: 0)
        }
        // 3、对当前数组进行排序(sortInPlace)直接对当前数组进行排序
        // 在swift中对尾随闭包，同时又返回值的还有一个简单的写法
        // 法一：
//        pakages[0].emoticons.sortInPlace { (emot1, emot2) -> Bool in
//            return emot1.times > emot2.times
//        }
        // 法二：
        // $0 对应的是第一个参数，$1 对应的是第二个参数，如有其它参数就一次类推，当然return也可以省略
        pakages[0].emoticons.sortInPlace { $0.times > $1.times }
//        printLog(pakages[0].emoticons as NSArray)
        
        // 删除多余的表情(倒数第二个)
        if pakages[0].emoticons.count > 21 {
            pakages[0].emoticons.removeAtIndex(19)
        }
        
        
        
    }
    
    ///  根据对应的indexPath 对应的表情模型
    ///
    ///  - parameter indexPath: indexPath
    ///
    ///  - returns: 表情模型
    func emoticon(indexPath: NSIndexPath) -> Emoticon {
        return pakages[indexPath.section].emoticons[indexPath.item]
    }
    
    // MARK: - 私有函数（加载表情包）
    private func loadPakages() {
        // 0、增加最近分组
        pakages.append(EmoticonPakage(dict: ["group_name_cn": "最近"]))
        
        // 1、读取emoticon.plist
        let path = NSBundle.mainBundle().pathForResource("emoticons.plist", ofType: nil, inDirectory: "Emoticons.bundle")
        // 2、读取字典
        let dict = NSDictionary(contentsOfFile: path!)
        // 3、获取pakages数组
        let array = dict!["packages"] as! [[String: AnyObject]]
        // 4、遍历数组，创建模型
        for infoDict in array {
            // 1> 获取 id，目录中对应的 info.plist 才是表情包的数据
            let id = infoDict["id"] as! String
            
            // 2> 拼接表情包路径
            let emPath = NSBundle.mainBundle().pathForResource("info.plist", ofType: nil, inDirectory: "Emoticons.bundle/" + id)
            
            // 3> 加载 info.plist 字典
            let packageDict = NSDictionary(contentsOfFile: emPath!) as! [String: AnyObject]
            print(packageDict)
            // 4> 字典转模型

            pakages.append(EmoticonPakage(dict: packageDict))
        }
//        print(pakages)
    }
    
    
}



    
