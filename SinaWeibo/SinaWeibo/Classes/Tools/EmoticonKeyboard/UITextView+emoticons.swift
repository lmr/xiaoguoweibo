//
//  UITextView+emoticons.swift
//  表情键盘
//
//  Created by 小果 on 16/2/27.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

extension UITextView {
    ///  计算性属性，返回完整的表情字符串
    var emoticonText: String {
        let attrText = attributedText
        // 创建一个可变字符串
        var strM = String()
        
        attrText.enumerateAttributesInRange(NSRange(location: 0, length: attrText.length), options: []) { (dict, range, _) -> Void in
            if let attachment = dict["NSAttachment"] as? EmoticonAttachment {
//                print("表情图片\(attachment.chs)")
                strM += attachment.chs
            }else {
                let str = (attrText.string as NSString).substringWithRange(range)
//                print("文本内容\(str)")
                strM += str
            }
        }
        return strM
    }
    
    ///  插入表情符号
    ///
    ///  - parameter emoticon: 表情符号模型
    func insertEmoticon(emoticon: Emoticon) {
        // 0、空的表情
        if emoticon.isEmpty {
            return
        }
        
        // 1、删除按钮
        if emoticon.isRemove {
            deleteBackward()
            return
        }
        print(emoticon)
        // 2、emoji
        if emoticon.emoji != nil {
            replaceRange(selectedTextRange!, withText: emoticon.emoji!)
            return
        }
        
        // 3、表情图片：一定有图片
        let imageText = EmoticonAttachment.emoticonAttributeText(emoticon, font: font!)
        
        
        
        // 1>从textView中取出属性文本
        let strM = NSMutableAttributedString(attributedString: attributedText)
        // 2>插入图片文字
        strM.replaceCharactersInRange(selectedRange, withAttributedString: imageText)
        // 3>重新设置textView的内容
        // 1)记录当前光标的位置
        let range = selectedRange
        // 2)设置内容
        attributedText = strM
        // 3)回复光标位置
        selectedRange = NSRange(location: range.location + 1, length: 0)
        
        // 4)执行代理方法
        delegate?.textViewDidChange!(self)
        
    }

    
    
}

