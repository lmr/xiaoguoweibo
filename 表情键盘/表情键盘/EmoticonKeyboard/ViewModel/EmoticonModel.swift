//
//  EmoticonModel.swift
//  表情键盘
//
//  Created by 小果 on 16/2/24.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

/// 表情的视图模型(用来加载表情数据),从Emoticon.bundle中读取emoticons.plist,遍历packes数组，创建EmoticonPakage数组，EmoticonPakage的明细内容，从id对应的目录加载info.plist完成字典转模型

class EmoticonModel: NSObject {

    ///  表情包的数组
    lazy var pakages = [EmoticonPakage]()
    
    // MARK: - 加载表情包
    func loadPakages() {
        // 0、增加最近分组
        pakages.append(EmoticonPakage(dict: ["group_name_cn": "最近"]))
        
        // 1、读取emoticon.plist
        let path = NSBundle.mainBundle().pathForResource("emoticons.plist", ofType: nil, inDirectory: "Emoticons.bundle")
        // 2、读取字典
        let dict = NSDictionary(contentsOfFile: path!)
        // 3、获取pakages数组
        let array = dict!["packages"] as! [[String: AnyObject]]
        // 4、遍历数组，创建模型
        for infoDict in array {
            // 1> 获取 id，目录中对应的 info.plist 才是表情包的数据
            let id = infoDict["id"] as! String
            
            // 2> 拼接表情包路径
            let emPath = NSBundle.mainBundle().pathForResource("info.plist", ofType: nil, inDirectory: "Emoticons.bundle/" + id)
            
            // 3> 加载 info.plist 字典
            let packageDict = NSDictionary(contentsOfFile: emPath!) as! [String: AnyObject]
            print(packageDict)
            // 4> 字典转模型

            pakages.append(EmoticonPakage(dict: packageDict))
        }
//        print(pakages)
    }
    
    ///  根据对应的indexPath 对应的表情模型
    ///
    ///  - parameter indexPath: indexPath
    ///
    ///  - returns: 表情模型
    func emoticon(indexPath: NSIndexPath) -> Emoticon {
     return pakages[indexPath.section].emoticons[indexPath.item]
    }
    
    
}



    
