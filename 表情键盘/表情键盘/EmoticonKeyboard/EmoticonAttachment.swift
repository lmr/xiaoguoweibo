//
//  EmoticonAttachment.swift
//  表情键盘
//
//  Created by 小果 on 16/2/26.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class EmoticonAttachment: NSTextAttachment {

    // 表情文字
    var chs: String
    
    init(chs: String) {
        self.chs = chs
        
        super.init(data: nil, ofType: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    ///  创建表情属性文本
    ///
    ///  - returns: 属性文本
    class func emoticonAttributeText(emoticon: Emoticon, font: UIFont) -> NSAttributedString {
        let attachment = EmoticonAttachment(chs: emoticon.chs!)
        
        attachment.image = UIImage(contentsOfFile: emoticon.imagePath)
        // 图片的高度
        let height = font.lineHeight
        // 切记：bounds的 X / Y 就是ScrollView的contentoffset，苹果利用bounds的X和Y可以调整控件内部的偏移位置
        attachment.bounds = CGRect(x: 0, y: -4, width: height, height: height)
        // 创建图片属性字符串
        let imageText = NSMutableAttributedString(attributedString: NSAttributedString(attachment: attachment))
        // 添加字体
        imageText.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: 1))
        
        return imageText
    }
    
}
