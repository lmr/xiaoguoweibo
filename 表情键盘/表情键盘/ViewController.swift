//
//  ViewController.swift
//  表情键盘
//
//  Created by 小果 on 16/2/23.
//  Copyright © 2016年 小果. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // 提取textView中的属性文字
    
    @IBAction func emojiClick(sender: AnyObject) {
        
        print(textView.emoticonText)
    }
    
    
    ///  表情键盘控制器(如果闭包中调用self.函数，同样会做一次copy，注意循环引用的问题)
    private lazy var keyBoardVC: EmoticonViewController = EmoticonViewController { [weak self] (emoticon) -> () in
        print(emoticon.chs)
        self?.textView.insertEmoticon(emoticon)
    }
        
    @IBOutlet weak var textView: UITextView!
    
    deinit {
        print("88")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.inputView = keyBoardVC.view
        
        let viewModel = EmoticonModel()
        viewModel.loadPakages()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        textView.becomeFirstResponder()
    }
}

